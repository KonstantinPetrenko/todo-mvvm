package com.todomvvm.kotlin.ui.authorization

data class SignInResult(
    val success: LoggedInUserView? = null,
    val error: String? = null
)
