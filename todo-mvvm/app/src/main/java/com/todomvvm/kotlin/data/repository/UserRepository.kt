package com.todomvvm.kotlin.data.repository

import com.todomvvm.kotlin.util.sealed.DestinationRoute
import com.todomvvm.kotlin.arch.SingleLiveEvent
import com.todomvvm.kotlin.ui.authorization.SignInResult

interface UserRepository {

    var eventSingIn: SingleLiveEvent<SignInResult>

    suspend fun isUserSignIn(): Boolean

    suspend fun signOut()

    suspend fun signIn(email: String, password: String)

    suspend fun signUp(email: String, password: String)

    suspend fun fetchUid(): String?
    val eventSignOut: SingleLiveEvent<DestinationRoute>
}