package com.todomvvm.kotlin.ui.main

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatEditText
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.todomvvm.kotlin.R
import com.todomvvm.kotlin.arch.BaseMainFragment
import com.todomvvm.kotlin.arch.LoadState
import com.todomvvm.kotlin.util.sealed.DestinationRoute
import com.todomvvm.kotlin.util.sealed.SignOutRoute
import com.todomvvm.kotlin.databinding.FragmentMainBinding
import com.todomvvm.kotlin.ui.main.adapters.TodoAdapter
import com.todomvvm.kotlin.ui.model.ExistTodo
import com.todomvvm.kotlin.ui.model.NewElementTodo
import com.todomvvm.kotlin.ui.model.Todo
import com.todomvvm.kotlin.util.Constants.SHOW_KEY_BOARD_DELAY
import com.todomvvm.kotlin.util.extensions.hideKeyboard
import com.todomvvm.kotlin.util.extensions.showKeyboard
import com.todomvvm.kotlin.util.sealed.Sorting
import kotlinx.android.synthetic.main.fragment_authorization.*
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.androidx.viewmodel.ext.android.getViewModel


class MainFragment : BaseMainFragment<FragmentMainBinding, MainViewModel>() {

    private var handler: Handler? = null

    private val todoAdapter: TodoAdapter by lazy {
        TodoAdapter(
            todoList = ArrayList(),
            onRemoveTodo = this@MainFragment::requestRemoveTodo,
            onMakeIsCompeted = this@MainFragment::requestIsCompletedTodo,
            onUpdateProduct = this@MainFragment::requestUpdateTodo)
    }

    override fun getCurrentDestinationId() = R.id.mainFragment
    override fun getBindingVariable() = null
    override fun initViewModel() = getViewModel<MainViewModel>()
    override fun getLayoutId() = R.layout.fragment_main

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).setSupportActionBar(mainToolbar)
        setListeners()
        initRecycler()
        viewModel.fetchTodoList(Sorting.SortingByAscending())
    }

    private fun initRecycler() {
        todoRecycler.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = todoAdapter
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.actionSortByCompletion -> {
                viewModel.fetchTodoListByStateCompleted(Sorting.SortingByCompleted(true))
            }
            R.id.actionSortByInCompletion -> {
                viewModel.fetchTodoListByStateCompleted(Sorting.SortingByCompleted(false))
            }
            R.id.actionSortAscending -> {
                viewModel.fetchTodoList(Sorting.SortingByAscending())
            }
            R.id.actionSortDescending -> {
                viewModel.fetchTodoList(Sorting.SortingByDescending())
            }
            R.id.actionSignOut -> {
                viewModel.signOut()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setListeners() {
        fabAddElement.setOnClickListener {
            viewModel.createElement()
        }
    }

    override fun setSubscribers() {
        viewModel.newElementTodo.observe(viewLifecycleOwner, Observer {
            createAddTodoDialogAndShow(R.string.add_new_element_todo, it)
        })

        viewModel.todoList.observe(viewLifecycleOwner, Observer {
            setTodoAdapter(it.toCollection(ArrayList()))
        })

        viewModel.updatedElement.observe(viewLifecycleOwner, Observer {
            todoAdapter.updateElement(it)
        })

        viewModel.eventSignOutRoute.observe(viewLifecycleOwner, Observer {
            navigateToRoute(it)
        })

        viewModel.state.observe(viewLifecycleOwner, Observer {
            when(it){
                is LoadState.Error -> {
                    showErrorMessage(it.errorMessage)
                    loading?.visibility = View.INVISIBLE
                }
                is LoadState.Loading -> {
                    loading?.visibility = View.VISIBLE
                }
                is LoadState.Ready -> {
                    loading?.visibility = View.INVISIBLE
                }
            }
        })
    }

    private fun setTodoAdapter(todoList: ArrayList<ExistTodo>) {
        todoAdapter.setTodoList(todoList)
    }

    private fun requestRemoveTodo(todo: ExistTodo) {
        viewModel.removeElement(todo)
    }

    private fun requestUpdateTodo(todo: ExistTodo){
        createAddTodoDialogAndShow(R.string.edit_element_todo, todo)
    }

    private fun requestIsCompletedTodo(todo: ExistTodo){
        viewModel.setStateCompleted(todo)
    }

    @SuppressLint("InflateParams")
    private fun createAddTodoDialogAndShow(@StringRes positiveBtn: Int, element: Todo) {

        val content: View =
            layoutInflater.inflate(R.layout.fragment_dialog_add_todo, null)

        val etTodo = content.rootView.findViewById<AppCompatEditText>(R.id.etTextTodo)

        if (element is ExistTodo){
            etTodo.setText(element.text)
        }
        val builderAddTodoDialog =
            AlertDialog.Builder(requireContext())

        builderAddTodoDialog.setPositiveButton(positiveBtn) { dialog, _ ->
            if (etTodo.text.toString().trim().isEmpty()) return@setPositiveButton

            when(element){
                is NewElementTodo -> {
                    element.text = etTodo.text.toString()
                    viewModel.addElement(element)
                }
                is ExistTodo -> {
                    element.text = etTodo.text.toString()
                    viewModel.updateElement(element)
                }
            }

            dialog.cancel()
            etTodo.rootView.hideKeyboard()
        }
        builderAddTodoDialog.setNegativeButton(R.string.cancel) { dialog: DialogInterface, _: Int ->
            dialog.cancel()
        }
        builderAddTodoDialog.setView(content)
        builderAddTodoDialog.create().show()

        Handler().postDelayed( {
            etTodo.requestFocus()
            etTodo.rootView.showKeyboard()
            }, SHOW_KEY_BOARD_DELAY)
    }

    private fun navigateToRoute(route: DestinationRoute) {
        when (route) {
            is SignOutRoute -> {
                navigate(MainFragmentDirections.actionMainFragmentToAuthFragment())
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        handler = null
    }
}