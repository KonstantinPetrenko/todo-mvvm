package com.todomvvm.kotlin.data.mapping

import com.todomvvm.kotlin.arch.mapper.Mapper
import com.todomvvm.kotlin.data.model.TodoEntity
import com.todomvvm.kotlin.ui.model.ExistTodo

class MapperTodoExist: Mapper<TodoEntity, ExistTodo> {

    override fun map(input: TodoEntity) =
        ExistTodo(
            uid = input.uid,
            completed = input.completed,
            text = input.text,
            timestamp = input.timestamp,
            key = input.key)
}