package com.todomvvm.kotlin.util.sealed

sealed class DestinationRoute

class AuthorizationRoute: DestinationRoute()
class MainRoute: DestinationRoute()
class SignOutRoute: DestinationRoute()
