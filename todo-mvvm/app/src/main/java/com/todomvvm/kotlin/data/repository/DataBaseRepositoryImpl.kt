package com.todomvvm.kotlin.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.todomvvm.kotlin.arch.LoadState
import com.todomvvm.kotlin.arch.SingleLiveEvent
import com.todomvvm.kotlin.arch.mapper.Mapper
import com.todomvvm.kotlin.data.model.TodoEntity
import com.todomvvm.kotlin.ui.model.ExistTodo
import com.todomvvm.kotlin.ui.model.NewElementTodo
import com.todomvvm.kotlin.ui.model.Todo
import com.todomvvm.kotlin.util.Constants
import com.todomvvm.kotlin.util.Constants.STATE_COMPLETED_TODO
import com.todomvvm.kotlin.util.sealed.Sorting


class DataBaseRepositoryImpl(
    private val database: FirebaseDatabase,
    private val mapperTodoToEntity: Mapper<Todo, TodoEntity>,
    private val mapperTodoToExist: Mapper<TodoEntity, ExistTodo>
) : DataBaseRepository {

    override val todoList = SingleLiveEvent<List<ExistTodo>>()
    override val updatedElement = SingleLiveEvent<ExistTodo>()
    override val state = SingleLiveEvent<LoadState>()

    init {
        database.setPersistenceEnabled(true)
    }

    override suspend fun addElement(uid: String, element: NewElementTodo) {
        state.postValue(LoadState.Loading())
        database
            .getReference(Constants.TODO_LIST_KEY)
            .child(uid)
            .push()
            .setValue(mapperTodoToEntity.map(element))
    }

    override suspend fun removeElement(element: ExistTodo) {
        state.postValue(LoadState.Loading())
        element.uid?.let {
            element.key?.let { key ->
                database
                    .getReference(Constants.TODO_LIST_KEY)
                    .child(it).child(key).removeValue()
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    override suspend fun fetchElements(sortingType: Sorting) {
        state.postValue(LoadState.Loading())

        FirebaseAuth.getInstance().currentUser?.uid?.let { uid ->
            database
                .getReference(Constants.TODO_LIST_KEY).child(uid)
                .addValueEventListener(object : ValueEventListener {

                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        state.postValue(LoadState.Ready())
                        val existTodoList = ArrayList<TodoEntity>()

                        dataSnapshot.children.forEach {

                            if (it.hasChildren()) {
                                it?.getValue(TodoEntity::class.java)
                                    ?.let { todoEntity ->
                                        todoEntity.key = it.key
                                        existTodoList.add(todoEntity)
                                    }
                            }
                        }
                        when(sortingType){
                            is Sorting.SortingByAscending -> {
                                todoList.postValue(existTodoList.map { entity ->
                                    mapperTodoToExist.map(entity)
                                })
                            }
                            is Sorting.SortingByDescending -> {
                                todoList.postValue(existTodoList.map { entity ->
                                    mapperTodoToExist.map(entity)
                                }.reversed())
                            }
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        state.postValue(LoadState.Error(databaseError.message))
                    }
                })
        }
    }

    override suspend fun updateElement(element: ExistTodo) {
        val entityTodo = mapperTodoToEntity.map(element)
        entityTodo.uid?.let {
            entityTodo.key?.let { key ->
                database
                    .getReference(Constants.TODO_LIST_KEY)
                    .child(it).updateChildren(mapOf(key to entityTodo))
                    .addOnSuccessListener {
                        state.postValue(LoadState.Ready())
                        updatedElement.postValue(mapperTodoToExist.map(entityTodo))
                    }
                    .addOnFailureListener {exception ->
                        state.postValue(exception.message?.let { it -> LoadState.Error(it) })
                    }
            }
        }
    }

    override suspend fun fetchTodoByCompleted(sortingType: Sorting) {
        FirebaseAuth.getInstance().currentUser?.uid?.let { uid ->
            database
                .getReference(Constants.TODO_LIST_KEY).child(uid).orderByChild(STATE_COMPLETED_TODO)
                .equalTo((sortingType as Sorting.SortingByCompleted).state)
                .addListenerForSingleValueEvent(object : ValueEventListener {

                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        state.postValue(LoadState.Ready())
                        val existTodoList = ArrayList<TodoEntity>()

                        dataSnapshot.children.forEach {

                            if (it.hasChildren()) {
                                it?.getValue(TodoEntity::class.java)
                                    ?.let { todoEntity ->
                                        todoEntity.key = it.key
                                        existTodoList.add(todoEntity)
                                    }
                            }
                        }
                        todoList.postValue(existTodoList.map { entity ->
                            mapperTodoToExist.map(entity)
                        })
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        state.postValue(LoadState.Error(databaseError.message))
                    }
                })
        }
    }
}