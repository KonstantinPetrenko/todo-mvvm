package com.todomvvm.kotlin.arch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.todomvvm.kotlin.ui.main.MainActivity
import com.todomvvm.kotlin.util.extensions.showError
import kotlinx.android.synthetic.main.fragment_authorization.*

abstract class BaseFragment<T : ViewDataBinding> : Fragment() {

    lateinit var binding: T
        private set

    @LayoutRes
    internal abstract fun getLayoutId(): Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        return binding.root
    }

    protected fun showErrorMessage(error: String) {
        (activity as? MainActivity)?.loading?.visibility = View.GONE
        context?.let {
            Snackbar.make(binding.root, error, Snackbar.LENGTH_SHORT).showError()
        }
    }
}