package com.todomvvm.kotlin.arch.mapper

interface Mapper<I, O> {

    fun map(input: I): O
}