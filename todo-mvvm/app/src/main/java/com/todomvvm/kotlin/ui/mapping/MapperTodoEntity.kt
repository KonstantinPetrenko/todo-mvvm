package com.todomvvm.kotlin.ui.mapping

import com.todomvvm.kotlin.arch.mapper.Mapper
import com.todomvvm.kotlin.data.model.TodoEntity
import com.todomvvm.kotlin.ui.model.ExistTodo
import com.todomvvm.kotlin.ui.model.NewElementTodo
import com.todomvvm.kotlin.ui.model.Todo
import java.lang.Exception

class MapperTodoEntity : Mapper<Todo, TodoEntity> {

    override fun map(input: Todo): TodoEntity {

        return when (input) {
            is NewElementTodo -> {
                 TodoEntity(
                    uid = input.uid,
                    completed = input.completed,
                    text = input.text,
                    timestamp = input.timestamp,
                     key = input.key
                )
            }
            is ExistTodo -> {
                 TodoEntity(
                    uid = input.uid,
                    completed = input.completed,
                    text = input.text,
                    timestamp = input.timestamp,
                     key = input.key
                )
            }
            else -> throw Exception("unknow type Todo ${input::javaClass}")
        }
    }
}