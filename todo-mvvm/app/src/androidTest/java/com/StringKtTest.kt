package com

import com.todomvvm.kotlin.util.extensions.isPasswordValid
import com.todomvvm.kotlin.util.extensions.isUserNameValid
import org.junit.Assert
import org.junit.Test

class StringKtTest {

        @Test
        fun checkIsValidPassword(){
            val password = "vnbdfhvbdfbvfhjvbfhjb"
            val password1Empty = ""
            val passwordNotValid = "12345"
            val passwordNotSix = "123456"
            val password2 = "000000000000000000"

            Assert.assertEquals(true, password.isPasswordValid())
            Assert.assertEquals(false, password1Empty.isPasswordValid())
            Assert.assertEquals(false, passwordNotValid.isPasswordValid())
            Assert.assertEquals(true, passwordNotSix.isPasswordValid())
            Assert.assertEquals(true, password2.isPasswordValid())
        }

    @Test
    fun checkIsValidLogin(){
        val loginSuccess = "bjdbhbdfjhb@gmail.com"
        val loginSuccess1 = "bjdbhbdfjhb@ukr.net"
        val loginError = "bjdbhbdfjhb@ukr"
        val loginError1 = "@ukr.net"
        val loginError2 = "bjdbhbdfjhbukr.net"
        val loginError3 = "bjdbhbdfjhb@gmailcom"
        val loginError4 = "bjdbhbdfjhb@.com"
        val loginError5 = ""

        Assert.assertEquals(true, loginSuccess.isUserNameValid())
        Assert.assertEquals(true, loginSuccess1.isUserNameValid())

        Assert.assertNotEquals(true, loginError.isUserNameValid())
        Assert.assertNotEquals(true, loginError1.isUserNameValid())
        Assert.assertNotEquals(true, loginError2.isUserNameValid())
        Assert.assertNotEquals(true, loginError3.isUserNameValid())
        Assert.assertNotEquals(true, loginError4.isUserNameValid())
        Assert.assertNotEquals(true, loginError5.isUserNameValid())
    }
}