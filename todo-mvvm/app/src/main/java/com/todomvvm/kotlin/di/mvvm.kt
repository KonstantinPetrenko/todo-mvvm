package com.todomvvm.kotlin.di

import com.todomvvm.kotlin.ui.authorization.AuthorizationViewModel
import com.todomvvm.kotlin.ui.main.MainViewModel
import com.todomvvm.kotlin.ui.splash.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mvvm = module {

    viewModel { SplashViewModel(get()) }
    viewModel { AuthorizationViewModel(get()) }
    viewModel { MainViewModel(get(), get()) }
}