package com

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.todomvvm.kotlin.data.repository.UserRepository
import junit.framework.Assert.assertNotNull
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.KoinComponent
import org.koin.core.inject

@RunWith(AndroidJUnit4::class)
class UserRepositoryTest: KoinComponent {

    private val userRepo by inject<UserRepository>()

    @Before
    fun setUp() {
    }

    @Test
     fun checkIsUidNotNullOrEmpty(){
        runBlocking {
           val resultUid = userRepo.fetchUid()
           assertNotNull(resultUid)
           assertNotEquals("", resultUid)
       }

    }
}