package com.todomvvm.kotlin.data.repository

import com.google.firebase.auth.FirebaseAuth
import com.todomvvm.kotlin.util.sealed.DestinationRoute
import com.todomvvm.kotlin.util.sealed.SignOutRoute
import com.todomvvm.kotlin.arch.SingleLiveEvent
import com.todomvvm.kotlin.ui.authorization.LoggedInUserView
import com.todomvvm.kotlin.ui.authorization.SignInResult
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class UserRepositoryImpl(private val auth: FirebaseAuth) :
    UserRepository {

    override var eventSingIn = SingleLiveEvent<SignInResult>()
    override val eventSignOut = SingleLiveEvent<DestinationRoute>()

    override suspend fun isUserSignIn() = auth.currentUser == null

    override suspend fun signOut() {
       auth.signOut()
        auth.addAuthStateListener {
            if (it.currentUser == null){
                eventSignOut.postValue(SignOutRoute())
            }
        }

    }

    override suspend fun signIn(email: String, password: String) {
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                eventSingIn.value = when {
                    task.isSuccessful -> {
                        SignInResult(LoggedInUserView(auth.currentUser?.email, password), null)
                    }
                    else -> {
                        SignInResult(null, task.exception?.message)
                    }
                }
            }
    }

    override suspend fun signUp(email: String, password: String) {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                when {
                    task.isSuccessful -> {
                        GlobalScope.launch {
                            signIn(email, password)
                        }
                    }
                    else -> {
                        eventSingIn.value = SignInResult(null, task.exception?.message)
                    }
                }
            }
    }

    override suspend fun fetchUid() = auth.currentUser?.uid ?: ""
}
