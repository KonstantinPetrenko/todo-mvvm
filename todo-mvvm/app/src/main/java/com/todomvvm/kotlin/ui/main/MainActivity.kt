package com.todomvvm.kotlin.ui.main

import androidx.appcompat.app.AppCompatActivity
import com.todomvvm.kotlin.R

class MainActivity : AppCompatActivity(R.layout.activity_main)