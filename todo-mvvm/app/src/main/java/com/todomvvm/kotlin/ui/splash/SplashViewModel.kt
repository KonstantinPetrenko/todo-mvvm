package com.todomvvm.kotlin.ui.splash

import com.todomvvm.kotlin.arch.BaseCoroutinesViewModel
import com.todomvvm.kotlin.util.sealed.DestinationRoute
import com.todomvvm.kotlin.arch.SingleLiveEvent
import com.todomvvm.kotlin.data.repository.UserRepository
import com.todomvvm.kotlin.util.Constants.SPLASH_SCREEN_DELAY
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SplashViewModel(private val userRepository: UserRepository): BaseCoroutinesViewModel() {


    val user = SingleLiveEvent<Boolean>()
    val nextRoute = SingleLiveEvent<DestinationRoute>()

    fun isUserSignIn(){
        scope.launch { user.postValue(userRepository.isUserSignIn()) }
    }

    fun navigateRoute(route: DestinationRoute){
        scope.launch {
            delay(SPLASH_SCREEN_DELAY)
            withContext(Dispatchers.Default) {
                nextRoute.postValue(route)
            }
        }
    }
}