package com.todomvvm.kotlin.util

import android.util.Log
import com.todomvvm.kotlin.BuildConfig

class Logger {
    companion object {
        private fun getClassName(any: Any): String {
            var mClassName = any.javaClass.name
            var mFirstPosition = mClassName.lastIndexOf(".") + 1
            if (mFirstPosition < 0) {
                mFirstPosition = 0
            }
            mClassName = mClassName.substring(mFirstPosition)
            mFirstPosition = mClassName.lastIndexOf("$")
            if (mFirstPosition > 0) {
                mClassName = mClassName.substring(0, mFirstPosition)
            }
            return mClassName
        }

        fun e(any: Any, message: String?) {
            if (BuildConfig.DEBUG)
                Log.e(any as? String ?: getClassName(any), message)
        }

        fun e(any: Any, message: String?, exception: Exception) {
            if (BuildConfig.DEBUG)
                Log.e(any as? String ?: getClassName(any), message, exception)
        }

        fun i(any: Any, message: String?) {
            if (BuildConfig.DEBUG)
                Log.i(any as? String ?: getClassName(any), message)
        }

        fun v(any: Any, message: String?) {
            if (BuildConfig.DEBUG)
                Log.v(any as? String ?: getClassName(any), message)
        }

        fun w(any: Any, message: String?) {
            if (BuildConfig.DEBUG)
                Log.w(any as? String ?: getClassName(any), message)
        }

        fun w(any: Any, message: String?, exception: Exception) {
            if (BuildConfig.DEBUG)
                Log.w(any as? String ?: getClassName(any), message, exception)
        }

        fun printStackTrace(e: Throwable?) {
            if (BuildConfig.DEBUG) {
                e?.printStackTrace()
            }
        }
    }
}