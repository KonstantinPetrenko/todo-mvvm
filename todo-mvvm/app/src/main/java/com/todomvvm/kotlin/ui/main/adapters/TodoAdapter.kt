package com.todomvvm.kotlin.ui.main.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.todomvvm.kotlin.R
import com.todomvvm.kotlin.databinding.ItemTodoBinding
import com.todomvvm.kotlin.ui.model.ExistTodo
import com.todomvvm.kotlin.util.extensions.notify

class TodoAdapter(
    private val todoList: ArrayList<ExistTodo>,
    private val onRemoveTodo: (ExistTodo) -> Unit,
    private val onMakeIsCompeted: (ExistTodo) -> Unit,
    private val onUpdateProduct: (ExistTodo) -> Unit
) : RecyclerView.Adapter<TodoAdapter.TodoHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoHolder {
        return TodoHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_todo, parent, false))
    }

    override fun getItemCount() = todoList.size

    override fun onBindViewHolder(holder: TodoHolder, position: Int) {
        holder.bind(todoList[position])
    }

    fun setTodoList(items: List<ExistTodo>) {
        notify(todoList, items) { left, right -> left.timestamp == right.timestamp }
        todoList.clear()
        todoList.addAll(items)
    }

    fun updateElement(pair: Pair<Int?, ExistTodo>){
        pair.first?.let {
            todoList[it] = pair.second
            notifyItemChanged(it)
        }
    }

    inner class TodoHolder(private val binding: ItemTodoBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(existTodo: ExistTodo) {
            binding.viewClickListener = View.OnClickListener(this::onClick)
            binding.todo = existTodo
            binding.executePendingBindings()
        }

        private fun onClick(view: View) {
            when (view.id) {
                R.id.ivCompleteState -> binding.todo?.let { onMakeIsCompeted.invoke(it) }
                R.id.ivRemove -> binding.todo?.let { onRemoveTodo.invoke(it) }
                R.id.ivEdit -> binding.todo?.let { onUpdateProduct.invoke(it) }
            }
        }
    }
}