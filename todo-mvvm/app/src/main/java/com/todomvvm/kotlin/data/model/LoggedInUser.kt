package com.todomvvm.kotlin.data.model


data class LoggedInUser(
        val userId: String,
        val displayName: String,
        val userMail: String)
