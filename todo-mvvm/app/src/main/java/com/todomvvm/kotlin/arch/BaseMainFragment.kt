package com.todomvvm.kotlin.arch

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.todomvvm.kotlin.util.Logger

abstract class BaseMainFragment<T : ViewDataBinding, V : ViewModel> :
    BaseViewModelFragment<T, V>() {

    abstract fun getCurrentDestinationId(): Int

    open fun navigate(directions: NavDirections) {
        if (findNavController().currentDestination?.id == getCurrentDestinationId()) {
            findNavController().navigate(directions)
        } else {
            Logger.e(this, "Unknown to this NavController")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
    }
}