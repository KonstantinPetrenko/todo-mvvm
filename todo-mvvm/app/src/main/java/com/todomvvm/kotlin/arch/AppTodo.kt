package com.todomvvm.kotlin.arch

import android.app.Application
import com.todomvvm.kotlin.di.app_module
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class AppTodo: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@AppTodo)
            modules(app_module)
        }
    }
}