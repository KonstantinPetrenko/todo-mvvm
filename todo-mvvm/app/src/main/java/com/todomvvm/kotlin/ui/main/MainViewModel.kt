package com.todomvvm.kotlin.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.todomvvm.kotlin.arch.BaseCoroutinesViewModel
import com.todomvvm.kotlin.arch.LoadState
import com.todomvvm.kotlin.util.sealed.DestinationRoute
import com.todomvvm.kotlin.arch.SingleLiveEvent
import com.todomvvm.kotlin.data.repository.DataBaseRepository
import com.todomvvm.kotlin.data.repository.UserRepository
import com.todomvvm.kotlin.ui.model.ExistTodo
import com.todomvvm.kotlin.ui.model.NewElementTodo
import com.todomvvm.kotlin.util.sealed.Sorting
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel(
    private val userRepository: UserRepository,
    private val dbRepository: DataBaseRepository
) : BaseCoroutinesViewModel() {

    val newElementTodo = SingleLiveEvent<NewElementTodo>()
    val todoList: SingleLiveEvent<List<ExistTodo>> = dbRepository.todoList
    val eventSignOutRoute: SingleLiveEvent<DestinationRoute> = userRepository.eventSignOut
    val state: SingleLiveEvent<LoadState> = dbRepository.state

    val updatedElement = Transformations.map(dbRepository.updatedElement){
        Pair(todoList.value?.indexOf(it), it)
    }

    fun signOut() {
        scope.launch { userRepository.signOut() }
    }

    private fun fetchUid() : String? {
        var uid: String? = null
        scope.launch(Dispatchers.Main.immediate) {
            uid = userRepository.fetchUid()
        }
        return uid
    }

    fun addElement(element: NewElementTodo){
        scope.launch {
            userRepository.fetchUid()?.let {
                dbRepository.addElement(it, element)
            }
        }
    }

    fun removeElement(element: ExistTodo){
        scope.launch {
            dbRepository.removeElement(element)
        }
    }

    fun createElement() {
        newElementTodo.postValue(NewElementTodo(
            uid = fetchUid() ?: "",
            completed = false,
            text = "",
            timestamp = System.currentTimeMillis()
        ))
    }

    fun updateElement(element: ExistTodo){
        scope.launch {
            dbRepository.updateElement(element)
        }
    }

    fun setStateCompleted(element: ExistTodo){
        element.completed = !element.completed
        scope.launch {
            dbRepository.updateElement(element)
        }
    }

    fun fetchTodoList(sortingType: Sorting){
        scope.launch { dbRepository.fetchElements(sortingType) }
    }

    fun fetchTodoListByStateCompleted(sortingType: Sorting){
        scope.launch { dbRepository.fetchTodoByCompleted(sortingType) }
    }
}