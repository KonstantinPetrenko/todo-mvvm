package com.todomvvm.kotlin.util.sealed

sealed class Sorting {

    data class SortingByCompleted(val state: Boolean) : Sorting()
    class SortingByAscending : Sorting()
    class SortingByDescending: Sorting()


}