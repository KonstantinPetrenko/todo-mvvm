package com.todomvvm.kotlin.arch

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel

abstract class BaseViewModelFragment<T : ViewDataBinding, V : ViewModel> : BaseFragment<T>() {

    lateinit var viewModel: V
        private set

    @LayoutRes
    internal abstract fun getBindingVariable(): Int?

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = initViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBindingVariable()?.let {
            binding.setVariable(it, viewModel)
        }
        binding.executePendingBindings()
        binding.lifecycleOwner = viewLifecycleOwner
        setSubscribers()
    }

    abstract fun initViewModel(): V

    protected abstract fun setSubscribers()
}