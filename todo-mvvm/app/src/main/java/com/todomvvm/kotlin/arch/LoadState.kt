package com.todomvvm.kotlin.arch

sealed class LoadState {
    class Loading() : LoadState()
    class Ready() : LoadState()
    class Error(val errorMessage: String) : LoadState()
}