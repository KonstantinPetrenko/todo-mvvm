package com.todomvvm.kotlin.di

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import org.koin.dsl.module

val firebase = module {
    single { FirebaseAuth.getInstance() }
    single { FirebaseDatabase.getInstance() }
}