package com.todomvvm.kotlin.di

import com.todomvvm.kotlin.arch.mapper.Mapper
import com.todomvvm.kotlin.data.mapping.MapperTodoExist
import com.todomvvm.kotlin.data.model.TodoEntity
import com.todomvvm.kotlin.data.repository.DataBaseRepository
import com.todomvvm.kotlin.data.repository.DataBaseRepositoryImpl
import com.todomvvm.kotlin.data.repository.UserRepository
import com.todomvvm.kotlin.data.repository.UserRepositoryImpl
import com.todomvvm.kotlin.ui.mapping.MapperTodoEntity
import com.todomvvm.kotlin.ui.model.ExistTodo
import com.todomvvm.kotlin.ui.model.Todo
import org.koin.core.qualifier.named
import org.koin.dsl.module

private const val MAPPER_TODO = "MAPPER_EXIST_TODO"
private const val MAPPER_ENTITY_TODO = "MAPPER_ENTITY_TODO"

val repo = module {

    single <UserRepository> { UserRepositoryImpl(get()) }

    single <DataBaseRepository>{ DataBaseRepositoryImpl(get(),
        get(named(MAPPER_TODO)),
        get(named(MAPPER_ENTITY_TODO))) }

    single <Mapper<Todo, TodoEntity>> (named(MAPPER_TODO)) { MapperTodoEntity() }

    single <Mapper<TodoEntity, ExistTodo>> (named(MAPPER_ENTITY_TODO)) { MapperTodoExist() }
}
