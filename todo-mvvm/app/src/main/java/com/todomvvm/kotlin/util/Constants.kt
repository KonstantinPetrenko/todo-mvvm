package com.todomvvm.kotlin.util

object Constants {

    const val SPLASH_SCREEN_DELAY = 1500L
    const val PASSWORD_MIN_LENGTH = 6
    const val TODO_LIST_KEY = "todoList"
    const val SHOW_KEY_BOARD_DELAY = 250L
    const val STATE_COMPLETED_TODO = "completed"
    const val MAIL_CHAR = '@'
}