package com.todomvvm.kotlin.ui.splash

import androidx.lifecycle.Observer
import com.todomvvm.kotlin.R
import com.todomvvm.kotlin.util.sealed.AuthorizationRoute
import com.todomvvm.kotlin.arch.BaseMainFragment
import com.todomvvm.kotlin.util.sealed.DestinationRoute
import com.todomvvm.kotlin.util.sealed.MainRoute
import com.todomvvm.kotlin.databinding.FragmentSplashBinding
import org.koin.androidx.viewmodel.ext.android.getViewModel

class SplashFragment : BaseMainFragment<FragmentSplashBinding, SplashViewModel>() {

    override fun getCurrentDestinationId() = R.id.splashFragment
    override fun getBindingVariable() = null
    override fun initViewModel() = getViewModel<SplashViewModel>()
    override fun getLayoutId() = R.layout.fragment_splash

    override fun setSubscribers() {
        viewModel.user.observe(viewLifecycleOwner, Observer {
            it.let {
                    viewModel.navigateRoute(if (it) AuthorizationRoute() else MainRoute())
            }
        })

        viewModel.nextRoute.observe(viewLifecycleOwner, Observer {
            navigateToRoute(it)
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.isUserSignIn()
    }

   private fun navigateToRoute(route: DestinationRoute){
        when(route){
            is AuthorizationRoute -> {
                navigate(SplashFragmentDirections.actionSplashFragmentToAuthFragment())
            }
            is MainRoute -> {
                navigate(SplashFragmentDirections.actionSplashFragmentToMainFragment())
            }
        }
    }
}