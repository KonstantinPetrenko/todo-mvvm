package com.todomvvm.kotlin.util

import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.todomvvm.kotlin.R

@BindingAdapter("setCompletedImage")
fun setCompletedImage(imageView: ImageView, isCompleted: Boolean){
    imageView.setImageDrawable(if (isCompleted)
        ContextCompat.getDrawable(imageView.context, R.drawable.ic_circle_blue_done)
     else ContextCompat.getDrawable(imageView.context, R.drawable.ic_circle_grey))
}