package com

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.todomvvm.kotlin.ui.main.MainViewModel
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.KoinComponent
import org.koin.core.inject

@RunWith(AndroidJUnit4::class)
class MainViewModelTest: KoinComponent {

    private val mainViewModel by inject<MainViewModel>()

    @Before
    fun setUp() {
    }

    @Test
    fun checkIsCreatedElement(){
        mainViewModel.createElement()
    }
}