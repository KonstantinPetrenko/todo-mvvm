package com.todomvvm.kotlin.ui.authorization

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import android.util.Patterns
import androidx.arch.core.util.Function
import androidx.lifecycle.Transformations
import androidx.lifecycle.liveData
import com.todomvvm.kotlin.R
import com.todomvvm.kotlin.arch.BaseCoroutinesViewModel
import com.todomvvm.kotlin.util.sealed.DestinationRoute
import com.todomvvm.kotlin.util.sealed.MainRoute
import com.todomvvm.kotlin.arch.SingleLiveEvent
import com.todomvvm.kotlin.data.repository.UserRepository
import com.todomvvm.kotlin.util.Constants.PASSWORD_MIN_LENGTH
import com.todomvvm.kotlin.util.extensions.isPasswordValid
import com.todomvvm.kotlin.util.extensions.isUserNameValid
import kotlinx.coroutines.launch


class AuthorizationViewModel(private val userRepository: UserRepository) :
    BaseCoroutinesViewModel() {

    val errorInputPassword = SingleLiveEvent<Int>()
    val errorInputUserName = SingleLiveEvent<Int>()
    val eventSignIn = SingleLiveEvent<Boolean>()
    val eventSignUp = SingleLiveEvent<Boolean>()

    private val _loginResultError = MutableLiveData<String>()
    val loginResultError: LiveData<String> = _loginResultError

    val eventRoute = SingleLiveEvent<DestinationRoute>()

    val signIn = userRepository.eventSingIn

    fun signIn(username: String, password: String) {
        setLoading(true)
        scope.launch { userRepository.signIn(username, password) }
    }

    fun resultSignIn(result: SignInResult) {
        if (result.success != null) {
            eventRoute.value = MainRoute()
        } else {
            _loginResultError.value = result.error
        }
    }

    fun signUp(username: String, password: String) {
        setLoading(true)
        scope.launch { userRepository.signUp(username, password) }
    }

    fun isValidAuthData(username: String, password: String, authType: Auth) {
        when(authType){
            Auth.SIGN_IN -> {
                eventSignIn.value = username.isUserNameValid() && password.isPasswordValid()
            }
            Auth.SIGN_UP -> {
                eventSignUp.value = username.isUserNameValid() && password.isPasswordValid()
            }
        }
    }

    fun loginDataChanged() {
        errorInputUserName.value = null
    }

    fun passwordDataChanged() {
        errorInputPassword.value = null
    }

    fun setErrorInputLayouts(username: String, password: String) {
        errorInputUserName.value = if (username.isEmpty()) R.string.empty_input_field
                                    else R.string.invalid_username
        errorInputPassword.value = if (password.isEmpty()) R.string.empty_input_field
                                    else R.string.invalid_password
    }
}
