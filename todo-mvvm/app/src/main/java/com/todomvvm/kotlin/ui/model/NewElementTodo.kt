package com.todomvvm.kotlin.ui.model

import androidx.annotation.Keep
import com.google.firebase.database.IgnoreExtraProperties


@Keep
@IgnoreExtraProperties
data class NewElementTodo(val uid: String? = null,
                          val completed: Boolean = false,
                          var text: String = "",
                          val timestamp: Long = 0,
                          var key: String? = ""): Todo {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as NewElementTodo

        if (timestamp != other.timestamp) return false

        return true
    }

    override fun hashCode(): Int {
        return timestamp.hashCode()
    }
}