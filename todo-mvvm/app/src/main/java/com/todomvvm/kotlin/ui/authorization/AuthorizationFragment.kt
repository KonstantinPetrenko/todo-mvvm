package com.todomvvm.kotlin.ui.authorization

import android.view.View
import androidx.lifecycle.Observer
import com.google.android.material.textfield.TextInputLayout
import com.todomvvm.kotlin.R
import com.todomvvm.kotlin.arch.BaseMainFragment
import com.todomvvm.kotlin.util.sealed.DestinationRoute
import com.todomvvm.kotlin.util.sealed.MainRoute
import com.todomvvm.kotlin.databinding.FragmentAuthorizationBinding
import com.todomvvm.kotlin.util.extensions.afterTextChanged
import kotlinx.android.synthetic.main.fragment_authorization.*
import org.koin.androidx.viewmodel.ext.android.getViewModel

enum class Auth {
    SIGN_IN, SIGN_UP
}

class AuthorizationFragment :
    BaseMainFragment<FragmentAuthorizationBinding, AuthorizationViewModel>() {

    override fun getCurrentDestinationId() = R.id.authFragment

    override fun getBindingVariable(): Int? = null
    override fun initViewModel() = getViewModel<AuthorizationViewModel>()
    override fun getLayoutId() = R.layout.fragment_authorization

    override fun setSubscribers() {

        viewModel.errorInputUserName.observe(viewLifecycleOwner, Observer {
            setErrorText(it, userNameInputLayout)
        })

        viewModel.errorInputPassword.observe(viewLifecycleOwner, Observer {
            setErrorText(it, passwordInputLayout)
        })

        viewModel.signIn.observe(viewLifecycleOwner, Observer {
            viewModel.resultSignIn(it)
        })

        viewModel.eventRoute.observe(viewLifecycleOwner, Observer {
            navigateToRoute(it)
        })

        viewModel.eventSignIn.observe(viewLifecycleOwner, Observer {
            if (it){
                signIn(userNameInputLayout?.editText?.text.toString(), password.text.toString())
            } else {
                viewModel.setErrorInputLayouts(
                    userNameInputLayout?.editText?.text.toString(),
                    password.text.toString())
            }
        })

        viewModel.eventSignUp.observe(viewLifecycleOwner, Observer {
            if (it){
                signUp(userNameInputLayout?.editText?.text.toString(), password.text.toString())
            } else {
                viewModel.setErrorInputLayouts(
                    userNameInputLayout?.editText?.text.toString(),
                    password.text.toString())
            }
        })

        viewModel.loginResultError.observe(viewLifecycleOwner, Observer {
            showErrorMessage(it)
        })

        viewModel.isLoading.observe(viewLifecycleOwner, Observer {
            loading?.visibility = if (it) View.VISIBLE else View.GONE
        })

        userNameInputLayout?.editText?.afterTextChanged {
            viewModel.loginDataChanged()
        }

        password.apply {
            afterTextChanged {
                viewModel.passwordDataChanged()
            }

            loginBtn?.setOnClickListener {
                checkValidAuthData(Auth.SIGN_IN)
            }

            registrationBtn?.setOnClickListener {
                checkValidAuthData(Auth.SIGN_UP)
            }
        }
    }

    private fun setErrorText(error: Int?, inputLayout: TextInputLayout) {
        inputLayout.error = if (error == null) error else getString(error)
    }

    private fun checkValidAuthData(authType: Auth) {
        viewModel.isValidAuthData(
            userNameInputLayout?.editText?.text.toString(),
            password.text.toString(), authType)
    }

    private fun navigateToRoute(route: DestinationRoute) {
        when (route) {
            is MainRoute -> {
                navigate(AuthorizationFragmentDirections.actionAuthFragmentToMainFragment())
            }
        }
    }

    private fun signIn(email: String, password: String) {
        loading.visibility = View.VISIBLE
        viewModel.signIn(email, password)
    }

    private fun signUp(email: String, password: String){
        loading.visibility = View.VISIBLE
        viewModel.signUp(email, password)
    }
}
