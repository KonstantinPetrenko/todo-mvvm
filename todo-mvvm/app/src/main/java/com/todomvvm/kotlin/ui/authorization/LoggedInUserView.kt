package com.todomvvm.kotlin.ui.authorization

data class LoggedInUserView(val mail: String?, val password: String?)
