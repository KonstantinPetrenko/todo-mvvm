package com.todomvvm.kotlin.util.extensions

import android.util.Patterns
import com.todomvvm.kotlin.util.Constants
import com.todomvvm.kotlin.util.Constants.MAIL_CHAR

fun String.isUserNameValid() =
        Patterns.EMAIL_ADDRESS.matcher(this).matches()


fun String.isPasswordValid() =
    this.length > Constants.PASSWORD_MIN_LENGTH