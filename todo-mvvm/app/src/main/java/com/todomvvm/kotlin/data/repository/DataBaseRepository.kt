package com.todomvvm.kotlin.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.todomvvm.kotlin.arch.LoadState
import com.todomvvm.kotlin.arch.SingleLiveEvent
import com.todomvvm.kotlin.ui.model.ExistTodo
import com.todomvvm.kotlin.ui.model.NewElementTodo
import com.todomvvm.kotlin.util.sealed.Sorting

interface DataBaseRepository {

    val todoList: SingleLiveEvent<List<ExistTodo>>

    val updatedElement: SingleLiveEvent<ExistTodo>

    val state: SingleLiveEvent<LoadState>

    suspend fun addElement(uid: String, element: NewElementTodo)

    suspend fun removeElement(element: ExistTodo)

    suspend fun fetchElements(sortingType: Sorting)

    suspend fun updateElement(element: ExistTodo)

    suspend fun fetchTodoByCompleted(sortingType: Sorting)
}